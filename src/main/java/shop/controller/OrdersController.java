package shop.controller;
/** @file OrdersController.java
* @brief Web controller when /orders page is requested
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import shop.model.Order;
import shop.model.User;
import shop.service.OrderDetailService;
import shop.service.UserService;
import shop.user.CrmUser;

@Controller
@RequestMapping("/orders")
public class OrdersController {

	@Autowired
	private UserService userService;

	@Autowired
	private OrderDetailService ordersDetailService;

	
	/**A method that executes when typing /orders/list direction
	*@param Model
	*@return Void method
	*/
	@GetMapping("/list")
	public String showOrdersTable(Model theModel) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		
		User user = userService.findByUserName(username);
		List<Order> orders = ordersDetailService.findOrdersByUserId(user.getId());
		theModel.addAttribute("orders", orders);
		theModel.addAttribute("crmUser", new CrmUser());

		return "orders";
	}
}
