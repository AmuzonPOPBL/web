package shop.controller;
/** @file MyController.java
* @brief Web controller when home page is requested
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import shop.model.Category;
import shop.model.Order;
import shop.model.Product;
import shop.model.User;
import shop.service.CategoryService;
import shop.service.OrderDetailService;
import shop.service.ProductService;
import shop.service.UserService;
import shop.user.CrmUser;

@Controller
public class MyContoller {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private OrderDetailService orderService;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserService userService;

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {

		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
	/**A method that executes when typing / direction,
	*and it redirects to home definition on tiles.xml,
	* which will redirect into a  view
	*@param Model
	*@return String
	*/
	@GetMapping("/")
	public String showHome(Model theModel) {
		List<Product> newProducts = productService.getLatestProducts();
		List<Category> categories = categoryService.getCategories();
		theModel.addAttribute("categories", categories);
		theModel.addAttribute("newProducts", newProducts);
		theModel.addAttribute("crmUser", new CrmUser());
		theModel.addAttribute("order", new Order());

		return "home";
	}
	
	/**A method that executes when typing /createOrder direction,
	*and it redirects to redirect:/ definition on tiles.xml,
	* which will redirect into a  view. It stores a order
	*@param Order order, int productID
	*@return String
	*/
	@PostMapping("/createOrder")
	public String saveOrder(@ModelAttribute("order") Order order,
			@RequestParam(value = "productId", required = true) int productId) {

		order.setProducts(new ArrayList<>());
		order.getProducts().add(productService.getProduct(productId));
		order.setUserID(userService.loginUser());
		order.setFecha(new Date());
		order.setDestino(1);
		order.setEstado("1");
		order.setRobotID(0);
		orderService.saveOrder(order);

		return "redirect:/";
	}
	
	/**A method that executes when typing /aboutUs direction,
	*and it redirects to aboutUs definition on tiles.xml,
	* which will redirect into a  view
	*@param No parameter
	*@return String
	*/
	@GetMapping("/aboutUs")
	public String showAboutUs(Model theModel) {

		theModel.addAttribute("crmUser", new CrmUser());

		return "aboutUs";
	}

	/**A method that executes when typing /contactUs direction,
	*and it redirects to contactUs definition on tiles.xml,
	* which will redirect into a  view
	*@param No parameter
	*@return String
	*/
	@GetMapping("/contactUs")
	public String showContactUs(Model theModel) {

		theModel.addAttribute("crmUser", new CrmUser());
		return "contactUs";
	}

	/**A method that executes when typing /processRegistrationForm direction,
	*and it redirects to contactUs home on tiles.xml,
	* which will redirect into a  view. Also saves a user
	*@param CrmUser theCrmUser, BindingResult theBindingResult, Model theModel
	*@return String
	*/
	@PostMapping("/processRegistrationForm")
	public String processRegistrationForm(@Valid @ModelAttribute("crmUser") CrmUser theCrmUser,
			BindingResult theBindingResult, Model theModel) {

		String userName = theCrmUser.getUserName();


		// form validation
		if (theBindingResult.hasErrors()) {
			return "home";
		}

		// check the database if user already exists
		User existing = userService.findByUserName(userName);
		if (existing != null) {
			theModel.addAttribute("crmUser", new CrmUser());
			theModel.addAttribute("registrationError", "User name already exists.");


			return "home";
		}
		// create user account
		userService.save(theCrmUser);



		return "home";
	}

}
