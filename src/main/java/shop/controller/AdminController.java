package shop.controller;

/** @file AdminController.java
* @brief Web controller when /admin direction is requested
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import shop.model.Category;
import shop.model.GetJSON;
import shop.model.Order;
import shop.model.Position;
import shop.model.Product;
import shop.model.Robot;
import shop.model.User;
import shop.service.CategoryService;
import shop.service.OrderDetailService;
import shop.service.PositionService;
import shop.service.ProductService;
import shop.service.RobotService;
import shop.service.UserService;
import shop.user.CrmUser;

@Controller
@RequestMapping("/admin")
public class AdminController {


	/**A method that executes when typing /admin/ direction,
	*and it redirects to admin definition on tiles
	*@param No parameter
	*@return String
	*/
	@GetMapping("")
	public String showSystems() {

		return "admin";
	}
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/user")
	public String controladorUser(Model theModel) {
		// get customers from the service
		List<User> theUsers = userService.getListUsers();

		// add the customers to the model
		theModel.addAttribute("users", theUsers);

		return "user";
	}
	
	@Autowired
	private OrderDetailService orderService;
	@GetMapping("/orders")
	public String controladorOrders(Model theModel) {
		// get customers from the service
		List<Order> theOrders = orderService.listOrders();
		List<User> theUsers = userService.getListUsers();
		
		theModel.addAttribute("users", theUsers);		
		theModel.addAttribute("orders", theOrders);

		return "allorders";
	}
	
	@Autowired
	private ProductService productService;
	@GetMapping("/productos")
	public String controladorProduts(Model theModel) {
		
		
		List<Product> theProduct = productService.getProducts();


		theModel.addAttribute("products", theProduct);

		return "productos";
	}
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private PositionService positionService;

	/**A method that executes when typing /admin/addProduct direction,
	*and it redirects to addProduct definition on tiles.xml,
	* which will redirect into a  view
	*@param No parameter
	*@return String
	*/
	@GetMapping("/addProduct")
	public String getAddProduct(Model theModel) {
		List<Position> temp = positionService.getPositions();
		List<Position> positions = new ArrayList<>();
		positions.add(temp.get(6));
		positions.add(temp.get(7));
		positions.add(temp.get(8));
		
		
		List<Category> categories = categoryService.getCategories();
		theModel.addAttribute("categories", categories);
		theModel.addAttribute("positions", positions);
		theModel.addAttribute("product", new Product());
		

		
		return "addProduct";
	}
	@PostMapping("/saveProduct")
	public String saveUser(@ModelAttribute("product") Product p, @RequestParam(value = "categoryID", required = true) String categoryID,
			@RequestParam(value = "positionId", required = true) int positionId) {
		
		p.setPositionID(positionService.getPosition(positionId));
		p.setCategory(categoryService.getCategoryByName(categoryID));
		p.setImg("/images/products/" + p.getCategory().getName().toLowerCase() + "/" + p.getName() + ".jpg");
		productService.saveProduct(p);
		
		return "redirect:/admin/addProduct";
	}
	@GetMapping("/addUser")
	public String getAddUser(Model theModel) {

		
		theModel.addAttribute("crmuser", new CrmUser());
		return "addUser";
	}
	
	@PostMapping("/saveUser")
	public String saveUser(@ModelAttribute("crmuser") CrmUser user) {

		userService.saveAdmin(user);
		
		
		return "redirect:/admin/addUser";
	}
	/**A method that executes when typing /admin/grafico direction,
	*and it redirects to grafico definition on tiles.xml,
	* which will redirect into a  view
	*@param No parameter
	*@return String
	*/
	@GetMapping("/grafico")
	public String getGrafico(Model theModel,HttpSession session) {
		 	 	
		GetJSON json = new GetJSON();
		JSONArray j = json.getJSONFromQuery("select * from product");
	
		// add the customers to the model
		session.setAttribute("jsonPrueba",j.toString());
	
		return "grafico";
	}
	
	@Autowired
	private RobotService robotService;
	
	@GetMapping("/robots")
	public String showRobots(Model theModel) {
		
		List<Robot> robots = robotService.getRobots();
		theModel.addAttribute("robots", robots);
			
		return "robots";
	}
}
