package shop.dao;

/** @file CategoryDaoImpl.java
* @brief Implementation of CategoryDao interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import shop.model.Category;



@Repository
public class CategoryDaoImpl implements CategoryDao {

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	/**A method that queries to the database asking for a list of Categories
	*@param No parameter
	*@return List<Category>
	*/
	public List<Category> getCategories() {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		 return (List<Category>) session.
				 createQuery("from Category", Category.class)
				 .getResultList();
	}

	/**A method that queries to the database trying to get a category according to a name
	*@param String name
	*@return Category
	*/
	@Override
	public Category getCategoryByName(String name) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		 return (Category) session.
				 createQuery("from Category where name='" + name + "'", Category.class).getSingleResult();
	}
	
}
