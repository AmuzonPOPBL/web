package shop.dao;
/** @file ProductDao.java
* @brief Create interface with DAO pattern for Product
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Product;

public interface ProductDao {
	public List<Product> getProducts();
	public void saveProduct(Product product);
	public Product getProduct(int idProducto);
	public List<Product> getLatestProducts();
}
