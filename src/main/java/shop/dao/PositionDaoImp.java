package shop.dao;
/** @file PositionDaoImp.java
* @brief Implementation of PositionDao interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import shop.model.Position;

@Repository

public class PositionDaoImp implements PositionDao {

	 @Autowired
	  private SessionFactory sessionFactory;
	 
	 /**A method that queries to the database trying to find positions
	    *@param No parameter
		*@return List<Position>
		*/
	@Override
	public List<Position> getPositions() {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		 return (List<Position>) session.
				 createQuery("from Position", Position.class)
				 .getResultList();
	}

	/**A method that queries to the database 
	  * trying to save a position
	  *@param Position position
	  *@return Void method
	  */
	@Override
	public void savePosition(Position position) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		session.saveOrUpdate(position);
		
	}

	/**A method that queries to the database trying to get 
	 * a position according to an id
	 *@param int idPosition
	 *@return Position position
	 */
	@Override
	public Position getPosition(int idPosition) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Position thePosition = session.get(Position.class, idPosition);
		
		return thePosition;
	}



}
