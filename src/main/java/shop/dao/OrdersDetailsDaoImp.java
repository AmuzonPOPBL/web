package shop.dao;
/** @file OrdersDetailsDao.java
* @brief Create interface with DAO pattern for Order
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import shop.model.Order;

@Repository
@Transactional
public class OrdersDetailsDaoImp implements OrdersDetailsDao{

	 @Autowired
	  private SessionFactory sessionFactory;
	 
	/**A method that queries to the database trying to get a order list by a user id
	*@param int userID
	*@return List<Order>
	*/
	@Override
	
	public List<Order> findOrdersByUserId(int userID) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		
		 return (List<Order>) session.
				 createQuery("from Order where userID='"+userID+"'", Order.class)
				 .getResultList();
	}
	
	/**A method that queries to the database trying to get all order list
	*@return List<Order>
	*/
	@Override

	public List<Order> listOrders() {
		System.out.println("hacer el query");
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		 return (List<Order>) session.
				 createQuery("from Order", Order.class)
				 .getResultList();
	}

	/**A method that queries to the database trying to save an order
	*/
	@Override
	public void saveOrder(Order orders) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		session.saveOrUpdate(orders);
		
	}
	
	

}
