package shop.dao;
/** @file RobotDaoImp.java
* @brief Implementation of RobotDao interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import shop.model.Robot;

@Repository
@Transactional
public class RobotDaoImp implements RobotDao{
	@Autowired
	  private SessionFactory sessionFactory;
	/**A method that queries to the database trying 
	*to get a list of robots
	*@param No parameter
	*@return List<Robots>
	*/
	@Override
	public List<Robot> getRobots() {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Query<Robot> theQuery = 
				session.createQuery("from Robot",
											Robot.class);
		
		// execute query and get result list
		List<Robot> robots = theQuery.getResultList();
				
		// return the results		
		return robots;
	}

	/**A method that queries to the database trying to save a robot
	*@param Robot robot
	*@return Void method
	*/
	@Override
	public void saveRobot(Robot robot) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		session.saveOrUpdate(robot);
		
	}
	/**A method that queries to the database trying
	*to get a robot according to its id
	*@param Robot robot
	*@return Robot
	*/
	@Override
	public Robot getRobot(int idRobot) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Robot theRobot = session.get(Robot.class, idRobot);
		
		return theRobot;
	}

}
