package shop.dao;
/** @file OrdersDetailsDao.java
* @brief Create interface with DAO pattern for Order
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Order;


public interface OrdersDetailsDao {
	List<Order> findOrdersByUserId(int userID);
	List<Order> listOrders();

	void saveOrder(Order orders);
}
