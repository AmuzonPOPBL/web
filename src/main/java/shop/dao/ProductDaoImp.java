package shop.dao;
/** @file ProductDaoImp.java
* @brief Implementation of ProductDao interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import shop.model.Product;

@Repository
@Transactional
public class ProductDaoImp implements ProductDao{
	 @Autowired
	  private SessionFactory sessionFactory;

	 /**A method that queries to the database trying
	    *to get a list of products
		*@param  No parameter
		*@return List<Products>
		*/
	@Override
	public List<Product> getProducts() {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Query<Product> theQuery = 
				session.createQuery("from Product",
											Product.class);
		
		// execute query and get result list
		List<Product> products = theQuery.getResultList();
				
		// return the results		
		return products;
	}
	
	/**A method that queries to the database trying to
	* get a list of products taking into account the date
	*@param No parameter
	*@return List<Product>
	*/
	@Override
	public List<Product> getLatestProducts() {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Query<Product> theQuery = 
				session.createQuery("from Product order by insert_date desc",
											Product.class);
		
		// execute query and get result list
		List<Product> products = theQuery.getResultList();
		List<Product> latestProducts = products.stream().limit(4).collect(Collectors.toList());			
		return latestProducts;
	}
	
	/**A method that queries to the database trying to save a product
	*@param Product product
	*@return Void method
	*/
	@Override
	public void saveProduct(Product product) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		session.saveOrUpdate(product);
	}

	/**A method that queries to the database trying to
	* get a product according to its id
	*@param int idProducto
	*@return Product
	*/
	@Override
	public Product getProduct(int idProducto) {
		Session session;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    session = sessionFactory.openSession();
		}
		Product theProduct = session.get(Product.class, idProducto);
		
		return theProduct;
	}
	
}
