package shop.model;
/** @file Position.java
* @brief Definition of Position object and relationed to the database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "position")
public class Position {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "id_ws")
	private String idWS;
	
	@Column(name = "id_sg")
	private String idSG;
	
	public Position() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdWS() {
		return idWS;
	}

	public void setIdWS(String idWS) {
		this.idWS = idWS;
	}

	public String getIdSG() {
		return idSG;
	}

	public void setIdSG(String idSG) {
		this.idSG = idSG;
	}

	@Override
	public String toString() {
		return "Position [idPosition=" + id + ", idWS=" + idWS + ", idSG=" + idSG + "]";
	}
	
	
}
