package shop.model;
/** @file Robot.java
* @brief Definition of Robot object and relationed to the database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "robot")
public class Robot {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idRobot;
	

	@Column(name = "robot_status")
	private String robot_state;
	
	////////////////////////////////////////
	@OneToOne
	@JoinColumn(name = "position_id")
	private Position positionID;

	public Long getIdRobot() {
		return idRobot;
	}

	public void setIdRobot(Long idRobot) {
		this.idRobot = idRobot;
	}

	public String getRobot_state() {
		return robot_state;
	}

	public void setRobot_state(String robot_state) {
		this.robot_state = robot_state;
	}

	public Position getPositionID() {
		return positionID;
	}

	public void setPositionID(Position positionID_1) {
		this.positionID = positionID_1;
	}

	@Override
	public String toString() {
		return "Robot [idRobot=" + idRobot + ", robot_state=" + robot_state + ", positionID_1=" + positionID + "]";
	}
	
}
