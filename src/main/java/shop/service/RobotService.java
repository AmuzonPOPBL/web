package shop.service;
/** @file RobotService.java
* @brief Interface that makes RobotDao interact with database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Robot;

public interface RobotService {
	public List<Robot> getRobots();
	public void saveRobot(Robot robot);
	public Robot getRobot(int idRobot);
}
