package shop.service;
/** @file RobotServiceImp.java
* @brief Implementation of RobotService interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.RobotDao;
import shop.model.Robot;

@Service
@Transactional
public class RobotServiceImp implements RobotService{

	@Autowired
	private RobotDao robotDAO;
	/**Make RobotDao execute getRobots() method
	*@param No parameter
	*@return List<Robot>
	*/
	@Override
	public List<Robot> getRobots() {
		return robotDAO.getRobots();
	}
	/**Make RobotDao execute saveRobot(robot) method
	*@param Robot robot
	*@return Void method
	*/
	@Override
	public void saveRobot(Robot robot) {
		robotDAO.saveRobot(robot);
		
	}
	/**Make RobotDao execute getRobot(idRobot) method
	*@param int idRobot
	*@return Robot
	*/
	@Override
	public Robot getRobot(int idRobot) {
		return robotDAO.getRobot(idRobot);
	}


	
}
