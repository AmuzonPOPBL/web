package shop.service;
/** @file ProductService.java
* @brief Interface that makes ProductDao interact with database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Product;

public interface ProductService {
	public List<Product> getProducts();
	public void saveProduct(Product product);
	public Product getProduct(int idProduct);
	public List<Product> getLatestProducts();
}
