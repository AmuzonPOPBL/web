package shop.service;
/** @file PositionService.java
* @brief Interface that makes PositionDao interact with database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Position;

public interface PositionService {
	public List<Position> getPositions();

	public void savePosition(Position position);

	public Position getPosition(int idPosition);

}
