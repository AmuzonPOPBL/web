package shop.service;
/** @file CategoryService.java
* @brief Interface that makes CategoryDao interact with database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Category;

public interface CategoryService {
	public List<Category> getCategories();


	public Category getCategoryByName(String categoryID);
}
