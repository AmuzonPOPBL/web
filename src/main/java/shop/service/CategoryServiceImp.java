package shop.service;
/** @file CategoryServiceImp.java
* @brief Implementation of CategoryService interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import shop.dao.CategoryDao;
import shop.model.Category;


@Service
@Transactional
public class CategoryServiceImp implements CategoryService {

	@Autowired
	private CategoryDao categoryDAO;

	/**Make CategoryDao execute getCategories() method
	*@param No parameter
	*@return List<Category>
	*/
	@Override
	@Transactional
	public List<Category> getCategories() {
		return categoryDAO.getCategories();
	}


	/**Make CategoryDao execute getCategoryByName(categoryID) method
	*@param String categoryID
	*@return Category
	*/
	@Override
	@Transactional
	public Category getCategoryByName(String categoryID) {
		// TODO Auto-generated method stub
		return categoryDAO.getCategoryByName(categoryID);
	}



	

	
	
	
}
