package shop.service;
/** @file OrderDetailService.java
* @brief Interface that makes OrderDetailsDao interact with database
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import shop.model.Order;

public interface OrderDetailService {

	public List<Order> findOrdersByUserId(int id);
	public List<Order> listOrders();
	public void saveOrder(Order orders);
}
