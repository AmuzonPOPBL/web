package shop.service;
/** @file ProductServiceImp.java
* @brief Implementation of ProductService interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.ProductDao;
import shop.model.Product;


@Service
@Transactional
public class ProductServiceImp implements ProductService{
	@Autowired
	private ProductDao productDAO;

	/**Make ProductDao execute getProducts() method
	*@param No parameter
	*@return List<Products>
	*/
	@Override
	@Transactional
	public List<Product> getProducts() {
		return productDAO.getProducts();
	}
	/**Make ProductDao execute saveProduct(product) method
	*@param Product product
	*@return Void method
	*/
	@Override
	@Transactional
	public void saveProduct(Product product) {
		productDAO.saveProduct(product);
		
	}

	/**Make ProductDao execute getProduct(idProduct) method
	*@param int idProduct
	*@return Product
	*/
	@Override
	@Transactional
	public Product getProduct(int idProduct) {
		return productDAO.getProduct(idProduct);
	}
	/**Make ProductDao execute getLatestProducts() method
	*@param No parameter
	*@return List<Product>
	*/
	@Override
	@Transactional
	public List<Product> getLatestProducts() {
		
		return productDAO.getLatestProducts();
	}

	
}
