package shop.service;
/** @file UserService.java
* @brief Interface that makes UserDao interact with database
* @authors
* Name  | Surname    | Email                                 |
* ------|------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import shop.model.User;
import shop.user.CrmUser;

public interface UserService extends UserDetailsService {

    User findByUserName(String userName);

    void save(CrmUser crmUser);
    
    void saveAdmin(CrmUser crmUser);
    
    List<User> getListUsers();
    
    User loginUser();
}
