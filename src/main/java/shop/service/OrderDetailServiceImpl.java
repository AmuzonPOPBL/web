package shop.service;
/** @file OrderDetailServiceImpl.java
* @brief Implementation of OrderDetailService interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.OrdersDetailsDao;
import shop.model.Order;


@Service
@Transactional
public class OrderDetailServiceImpl implements OrderDetailService {
	@Autowired
	private OrdersDetailsDao ordersDAO;
	/**Make OrderDetailsDao execute findOrdersByUserId(id) method
	*@param Long id
	*@return List<Order>
	*/
	@Override
	@Transactional
	public List<Order> findOrdersByUserId(int id) {
		return ordersDAO.findOrdersByUserId(id);
	}
	/**Make OrderDetailsDao execute listMethod() method
	*@param No parameter
	*@return List<Order>
	*/
	@Override
	@Transactional
	public List<Order> listOrders() {
		return ordersDAO.listOrders();
	}

	/**Make OrderDetailsDao execute saveOrder(orders) method
	*@param Order orders
	*@return Void method
	*/
	@Override
	public void saveOrder(Order orders) {
		ordersDAO.saveOrder(orders);
		
	}
	
}
