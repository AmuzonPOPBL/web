package shop.service;
/** @file PositionServiceImp.java
* @brief Implementation of PositionService interface methods
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shop.dao.PositionDao;
import shop.model.Position;

@Service
@Transactional
public class PositionServiceImp implements PositionService {

	@Autowired
	private PositionDao positionDAO;
	/**Make PositionDao execute getPositions() method
	*@param No parameter
	*@return List<Position>
	*/
	@Override
	@Transactional
	public List<Position> getPositions() {
		return positionDAO.getPositions();
	}
	/**Make PositionDao execute savePosition(Position position) method
	*@param Position position
	*@return Void method
	*/
	@Override
	@Transactional
	public void savePosition(Position position) {
		positionDAO.savePosition(position);
	}
	/**Make PositionDao execute getPosition(id) method
	*@param int idPosition
	*@return Position
	*/
	@Override
	@Transactional
	public Position getPosition(int idPosition) {
		return positionDAO.getPosition(idPosition);
	}

	
}
