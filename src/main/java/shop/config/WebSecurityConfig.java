package shop.config;
/** @file WebSecurityConfig.java
* @brief Configuration of web security
* @authors
* Name  | Surname    | Email                                 |
* ------|--------    | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 07/01/2019
*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import shop.service.UserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;
	 
    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.authenticationProvider(authenticationProvider());
	  }

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
			.antMatchers("/resources/**").permitAll()
			.antMatchers("/").permitAll()
			.antMatchers("/orders/**").authenticated()
			.antMatchers("/admin").hasAnyRole("MANAGER", "ADMIN")
			.antMatchers("/admin/user").hasAnyRole("MANAGER", "ADMIN")
			.antMatchers("/admin/orders").hasAnyRole("MANAGER", "ADMIN")
			.antMatchers("/admin/robots").hasAnyRole("MANAGER", "ADMIN")
			.antMatchers("/admin/productos").hasAnyRole("MANAGER", "ADMIN")
			.antMatchers("/admin/**").hasRole("ADMIN")
			.and()
			.formLogin()
				.loginPage("/")
				.loginProcessingUrl("/authenticateTheUser")
				.successHandler(customAuthenticationSuccessHandler)
				.permitAll()
			.and()
			.logout().permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/access-denied");
		
	}
	
	 @Bean
	 public BCryptPasswordEncoder passwordEncoder() {
	 return new BCryptPasswordEncoder();
	 }
	 
	 //authenticationProvider bean definition
	 @Bean
	 public DaoAuthenticationProvider authenticationProvider() {
	 DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
	 auth.setUserDetailsService(userService);
	 auth.setPasswordEncoder(passwordEncoder());
	 return auth;
	 }
	
	
}