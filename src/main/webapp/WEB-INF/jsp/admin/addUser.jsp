<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  

	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Add User</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Add a User</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									<form:form action="saveUser" modelAttribute="crmuser" method="POST" id="form1">
										<div class="form-group">
											<label>Username:</label>
											<form:input class="form-control" path="userName" />
										
											<label>Password:</label>
											<form:input class="form-control" path="password" />
											
											<label>Repeat Password:</label>
											<form:input class="form-control" path="matchingPassword" />
										
											<label>First Name:</label>
											<form:input class="form-control" path="firstName" />
										
											<label>Last Name:</label>
											<form:input class="form-control" path="lastName" />
										
											<label>Email:</label>
											<form:input class="form-control" path="email" />
										</div>
									
										<p><button type="submit" form="form1" value="Submit" class="btn btn-success">Save</button></p>
										
									</form:form>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
