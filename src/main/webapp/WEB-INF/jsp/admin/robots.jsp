<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Robots</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Position of Robots:</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Robot ID</th>
											<th>Position Work Station</th>
											<th>Position Segment</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach items="${robots}" var="robot">
											<tr class="odd gradeX">
												<td><c:out value="${robot.idRobot}" /></td>
												<td><c:choose>
														<c:when test="${empty robot.positionID.idWS}">
        None
    </c:when>
														<c:otherwise>
       ${robot.positionID.idWS}
    </c:otherwise>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${empty robot.positionID.idSG}">
        None
    </c:when>
														<c:otherwise>
       ${robot.positionID.idSG}
    </c:otherwise>
													</c:choose></td>

											</tr>
										</c:forEach>

									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>

	</div>
	<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<!-- /#wrapper -->

<!-- jQuery -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery2.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/dataTables/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/js/dataTables/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/startmin.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive : true
		});
	});
</script>
