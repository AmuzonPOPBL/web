
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://d3js.org/d3.v5.min.js"></script>
<script>

	
		$(document).ready(
				function() {
					var miArray =<%=session.getAttribute("jsonPrueba")%>;
					var svg = d3.select("#chart1").append("svg")
							.attr("height", "600").attr("width", "1000");

					svg.selectAll("rect").data(miArray).enter().append("rect")
							.attr("height", function(d, i) {
								return (d.price/2)
							}).attr("width", "50").attr("x", function(d, i) {
								return (i * 60) + 25
							}).attr("y", function(d, i) {
								return 600 - (d.price/2)
							});
					// Select, append to SVG, and add attributes to text

					svg.selectAll("text")
				    .data(miArray)
				    .enter().append("text")
				    .text(function(d) {return d.name})
				           .attr("class", "text")
				           .attr("x", function(d, i) {return (i * 60) + 16})
				           .attr("y", function(d, i) {return  600 -(d.price/2)-2});
				  

				});
	</script>


<script>
	$(document).ready(function() {
		var miArray =<%=session.getAttribute("jsonPrueba")%>;

			console.log(miArray);
		
		var width = 600,
	    height = 300,
	    radius = Math.min(width, height) / 2;

	  var color = d3.scaleOrdinal(d3.schemePastel1);

	var arc = d3.arc()
	    .outerRadius(radius - 10)
	    .innerRadius(0);

	var labelArc = d3.arc()
	    .outerRadius(radius - 40)
	    .innerRadius(radius - 40);

	var pie = d3.pie()
	    .value(function(d) { return d.price; });

	var svg = d3.select("#chart2").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	  .append("g")
	    .attr("transform", "translate(" + (width / 2) + "," + (height / 2) + ")");

	

	  svg.datum(miArray);

	  var g = svg.selectAll(".arc").data(pie(miArray));
	//console.log("End angle from data: " + 180 * pie(data)[0].endAngle / Math.PI);

	    var gEnter = g.enter().append("g")
	      .attr("class", "arc");
	      gEnter.append('path');
	      gEnter.append('text');

	//console.log("End angle from data bound to .arc: " + 180 * svg.selectAll('.arc').data()[0].endAngle / Math.PI);

	      g.exit().remove();

	// works with these uncommented, but shouldn't be needed
	        svg.selectAll('.arc path').data(function(d, i) { return pie(d); });
	        svg.selectAll('.arc text').data(function(d, i) { return pie(d); });
	        
	      svg.selectAll('.arc path').attr("d", arc)
	      .style("fill", function(d) { return color(d.data.name); });

	    svg.selectAll('.arc text')
	      .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
	      .attr("dy", ".35em")
	      .text(function(d) {
	        var arcSize = d.endAngle - d.startAngle;
	        arcSize = arcSize * (180 / Math.PI);
	            return d.data.name;
	      });


		// Select, append to SVG, and add attributes to text
	});
	</script>




<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tables</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">DataTables Advanced Tables</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div id="chart1"></div>
					</div>
				</div>
			</div>

			<!-- /.table-responsive -->
		</div>
		<!-- /.panel-body -->
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tables</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">DataTables Advanced Tables</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div id="chart2"></div>
					</div>
				</div>
			</div>

			<!-- /.table-responsive -->
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->

</div>
<!-- /.col-lg-12 -->
