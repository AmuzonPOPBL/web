<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">New Product</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Add a product</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									<form:form action="saveProduct" modelAttribute="product"
										method="POST" id="form1">

										<div class="form-group">
											<label>Name:</label>
											<form:input class="form-control" path="name" />

											<label>Category</label> <select name="categoryID" class="form-control">
												<c:forEach var="category" items="${categories}">
													<option>${category.name}</option>
												</c:forEach>
											</select>
											
													
											<label>Position</label> <select name="positionId" class="form-control">
												<c:forEach var="position" items="${positions}">
													<option>${position.id}</option>
												</c:forEach>
											</select>
											
											 
											<label>Units:</label>
											<form:input class="form-control" path="unidades" />


											<label>Price:</label>
											<div class="form-group input-group">

												<form:input class="form-control" path="price" />
												<span class="input-group-addon"><i class="fa fa-usd"></i></span>
											</div>
											
										</div>
										<p>
											<button type="submit" form="form1" value="Submit"
												class="btn btn-success">Save</button>
										</p>
									</form:form>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
