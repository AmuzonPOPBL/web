<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Orders</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Orders</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-example">
								<thead>
									<tr>
										<th>Username</th>
										<th>Order Id</th>
										<th>Date</th>
										<th>Destination</th>
										<th>Status</th>
										<th>Robot Id</th>
										<th>Products</th>

									</tr>
								</thead>
								<tbody>

									<c:forEach items="${users}" var="user">
										<c:forEach items="${user.orders}" var="order">
											<tr class="odd gradeX">

												<td><c:out value="${user.userName}" /></td>
												<td><c:out value="${order.id}" /></td>
												<td><fmt:formatDate value="${order.fecha}"
														pattern="yyyy-MM-dd" /></td>
												<td><c:out value="${order.destino}" /></td>
												<td>
													<c:choose>
														<c:when test="${order.estado=='1'}">
									   						Pending
									   					</c:when>
														<c:otherwise>
										    				Sent
										    			</c:otherwise>
													</c:choose>
												</td>
												<td><c:choose>
														<c:when test="${order.robotID=='0'}">
									   						Not Assigned
									   					</c:when>
														<c:otherwise>
										    				${order.robotID}
										    			</c:otherwise>
													</c:choose></td>
												<td><c:forEach var="product" items="${order.products}">
														<c:out value="${product.name}" />
													</c:forEach></td>

											</tr>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>

	</div>
	<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<!-- /#wrapper -->

<!-- jQuery -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery2.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/dataTables/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/js/dataTables/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script
	src="${pageContext.request.contextPath}/resources/admin/js/startmin.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive : true
		});
	});
</script>
