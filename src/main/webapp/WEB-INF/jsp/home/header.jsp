
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal fade" id="myModal88" tabindex="-1" role="dialog"
	aria-labelledby="myModal88" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<security:authorize access="isAnonymous()">
					<h4 class="modal-title" id="myModalLabel">Don't Wait, Login
						now!</h4>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<h4 class="modal-title" id="myModalLabel">
						Welcome,
						<security:authentication property="principal.username" />
						!
					</h4>
				</security:authorize>

			</div>
			<div class="modal-body modal-body-sub">
				<div class="row">
					<div class="col-md-8 modal_body_left modal_body_left1"
						style="border-right: 1px dotted #C2C2C2; padding-right: 3em;">
						<div class="sap_tabs">
							<div id="horizontalTab"
								style="display: block; width: 100%; margin: 0px;">
								<security:authorize access="isAnonymous()">
									<ul>
										<li class="resp-tab-item"><span>Sign in</span></li>
										<li class="resp-tab-item"><span>Sign up</span></li>
									</ul>
								</security:authorize>
								<security:authorize access="isAuthenticated()">
									<ul>
										<li class="resp-tab-item"><span>What would you
												like to do?</span></li>
									</ul>
								</security:authorize>
								<div class="tab-1 resp-tab-content">
									<div class="facts">
										<div class="register">
											<security:authorize access="isAnonymous()">

												<form:form
													action="${pageContext.request.contextPath}/authenticateTheUser"
													method="post">
													<c:if test="${param.error != null}">
														<script>
															$('#myModal88')
																	.modal(
																			'show');
														</script>

														<div class="alert alert-danger">
															<strong>ATENCIÓN!</strong> Usuario o contraseña
															incorrectos.
														</div>
													</c:if>
													<input placeholder="Username" name="username" type="text">
													<input placeholder="Password" name="password"
														type="password">
													<div class="sign-up">
														<input type="submit" value="Sign in" />
													</div>
												</form:form>
											</security:authorize>
											<security:authorize access="isAuthenticated()">
												<form:form
													action="${pageContext.request.contextPath}/logout"
													method="post">
													<div class="sign-up">
														<input type="submit" value="Logout" />
													</div>
												</form:form>
												<security:authorize access="isAuthenticated()">
													<form action="/orders/list">
														<input type="submit" value="historial" />
													</form>
												</security:authorize>
												<security:authorize
													access="hasRole('ADMIN') or hasRole('MANAGER')">
													<form action="/admin">
														<input type="submit" value="admin" />
													</form>
												</security:authorize>
											</security:authorize>
										</div>
									</div>
								</div>
								<div class="tab-2 resp-tab-content">
									<div class="facts">
										<div class="register">
											<form:form
												action="${pageContext.request.contextPath}/processRegistrationForm"
												modelAttribute="crmUser" class="form-horizontal">
												<div class="register">
													<form:errors path="userName" cssClass="alert alert-danger" element="div"/>
													<form:input path="userName" placeholder="Username" />

													<form:errors path="password" cssClass="alert alert-danger" element="div"/>
													<form:password path="password" placeholder="Password" />

													<form:errors path="matchingPassword" cssClass="alert alert-danger" element="div"/>
													<form:password path="matchingPassword"
														placeholder="Confirm Password" />

													<form:errors path="firstName" cssClass="alert alert-danger" element="div"/>
													<form:input path="firstName" placeholder="First Name" />

													<form:errors path="lastName" cssClass="alert alert-danger" element="div"/>
													<form:input path="lastName" placeholder="Last Name" />

													<form:errors path="email" cssClass="alert alert-danger" element="div"/>
													<form:input path="email" placeholder="Email" />

													<div class="sign-up">
														<input type="submit" value="Register" />
													</div>
												</div>



											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script>
							$(document).ready(function() {
								$('#horizontalTab').easyResponsiveTabs({
									type : 'default', //Types: default, vertical, accordion           
									width : 'auto', //auto or any width like 600px
									fit : true
								// 100% fit in a container
								});
							});
						</script>
						<div id="OR" class="hidden-xs">OR</div>
					</div>
					<div class="col-md-4 modal_body_right modal_body_right1">
						<div class="row text-center sign-with">
							<div class="col-md-12">
								<h3 class="other-nw">Sign in with</h3>
							</div>
							<div class="col-md-12">
								<ul class="social">
									<li class="social_facebook"><a href="#"
										class="entypo-facebook"></a></li>
									<li class="social_dribbble"><a href="#"
										class="entypo-dribbble"></a></li>
									<li class="social_twitter"><a href="#"
										class="entypo-twitter"></a></li>
									<li class="social_behance"><a href="#"
										class="entypo-behance"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<security:authorize access="!isAuthenticated()">
	<script>
		$('#myModal88').modal('show');
	</script>
</security:authorize>
<!-- header modal -->
<!-- header -->
<div class="header" id="home1">
	<div class="container">
		<div class="w3l_login">
			<a href="#" data-toggle="modal" data-target="#myModal88"><span
				class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
		</div>
		<div class="w3l_logo">
			<img
				src="${pageContext.request.contextPath}/resources/images/logo.png"
				alt="Logo" />
		</div>

	</div>
</div>
