<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="table-responsive">

<table class="table table-striped table-bordered table-hover"
	id="dataTables-example">
	<thead>
		<tr>
			<th>Id</th>
			<th>Order Date</th>
			<th>Destination</th>
			<th>Status</th>
			<th>Robot ID</th>
			<th>Products</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${orders}" var="order">
		<tr class="odd gradeX">
			
			<td><c:out value="${order.id}"/></td>
			<td><fmt:formatDate value="${order.fecha}" pattern="yyyy-MM-dd" /></td>
			<td><c:out value="${order.destino}"/></td>
			<td>
			
				<c:choose>
   					<c:when test="${order.estado=='1'}">
   						Pending
   					</c:when>    
	    			<c:otherwise>
	    				Sent
	    			</c:otherwise>
				</c:choose>
			
			</td>
			<td>
				<c:choose>
   					<c:when test="${order.robotID=='0'}">
   						Not Assigned
   					</c:when>    
	    			<c:otherwise>
	    				${order.robotID}
	    			</c:otherwise>
				</c:choose>
			</td>
			<td><c:forEach var="product" items="${order.products}"><c:out value="${product.name}"/></c:forEach></td>
		</tr>
	</c:forEach>
	</tbody>
</table>
</div>