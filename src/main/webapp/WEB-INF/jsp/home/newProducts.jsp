<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<div class="new-products">
	<div class="container">
		<div class="col-md-12 wthree_banner_bottom_right">
			<h3>New Products</h3>
			<div class="agileinfo_new_products_grids">
				<c:forEach var="newProduct" items="${newProducts}">
					<div class="col-md-3 agileinfo_new_products_grid">
						<div class="agile_ecommerce_tab_left agileinfo_new_products_grid1">
							<div class="hs-wrapper hs-wrapper1">
								<img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> <img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> <img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> <img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> <img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> <img
									src="${pageContext.request.contextPath}/resources${newProduct.img}"
									alt="${newProduct.name}" style="width:300px;height:auto;" class="img-responsive" /> 

								<div class="w3_hs_bottom w3_hs_bottom_sub">
									<ul>
										<li><a href="#" data-toggle="modal"
											data-target="#myModal2"><span
												class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
										</li>
									</ul>
								</div>
							</div>
							<h5>
								<a>${newProduct.name}</a>
							</h5>
							<div class="simpleCart_shelfItem">
								<p>
									<i class="item_price">${newProduct.price} $</i>
								</p>
								<form:form action="createOrder" modelAttribute="order"
									method="post">
									<input type="hidden" name="productId" value="${newProduct.id}">
									<form:input path="id" type="hidden" value="0" />
									<security:authorize access="isAuthenticated()">
										<button type="submit" class="w3ls-cart">Add to cart</button>
									</security:authorize>

								</form:form>
							</div>
						</div>
					</div>

				</c:forEach>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>