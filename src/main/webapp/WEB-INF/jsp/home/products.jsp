<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<div class="banner-bottom">
	<div class="container">
		<div class="col-md-12 wthree_banner_bottom_right">
			<div class="bs-example bs-example-tabs" role="tabpanel"
				data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<c:forEach varStatus="stat" var="category" items="${categories}">
						<li role="presentation" ${stat.first ? 'class="active"' : ''}><a
							href="#${category.name}" id="${category.name}-tab" role="tab" data-toggle="tab"
							aria-controls="${category.name}">${category.name}</a></li>
					</c:forEach>

				</ul>

				<div id="myTabContent" class="tab-content">
					<c:forEach varStatus="stat" var="category" items="${categories}">

						<div role="tabpanel" class="tab-pane fade ${stat.first ? 'active' : ''} in" 
							id="${category.name}" aria-labelledby="${category.name}-tab">

							<div class="agile_ecommerce_tabs">
								<c:forEach var="product" items="${category.products}">
									<div class="col-md-4 agile_ecommerce_tab_left">
										<div class="hs-wrapper">
											<img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" /> <img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" /> <img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" /> <img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" /> <img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" /> <img
												src="${pageContext.request.contextPath}/resources${product.img}"
												alt="${product.name}" class="img-responsive" />
											<div class="w3_hs_bottom">
												<ul>
													<li><a href="#" data-toggle="modal"
														data-target="#myModal"><span
															class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<h5>
											<a href="single.html">${product.name}</a>
										</h5>
										<div class="simpleCart_shelfItem">
											<p>
												<i class="item_price">${product.price} $</i>
											</p>
											<form:form action="createOrder" modelAttribute="order" method="post">
												<input type="hidden" name="productId" value="${product.id}">
												<form:input path="id" type="hidden"  value="0" />  
												
												<security:authorize access="isAuthenticated()">
												<button type="submit" class="w3ls-cart">Add to cart</button>
												</security:authorize>
												
											</form:form>
										</div>
									</div>
								</c:forEach>
								<div class="clearfix"></div>
							</div>

						</div>

					</c:forEach>

				</div>

			</div>
		</div>
	</div>
</div>