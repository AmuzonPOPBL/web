
<img src="${pageContext.request.contextPath}/resources/images/contactUs.png" alt="Contact Us" class="centerContact">



<div class="containerUs">

	
	<div class="box">
		<div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
		<div class='details'><h3>Mondragon</h3></div>
	</div>
	
	<div class="box">
		<div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
		<div class='details'><h3>+34 666777888</h3></div>
	</div>
	
	<div class="box">
		<div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
		<div class='details'><h3>info@mondragon.edu</h3></div>
	</div>
</div>