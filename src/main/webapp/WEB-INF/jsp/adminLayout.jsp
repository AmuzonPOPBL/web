<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
rect {
	fill: #0080FF;
}

rect:hover {
	fill: red;
}

</style>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="${pageContext.request.contextPath}/resources/admin/css/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="${pageContext.request.contextPath}/resources/admin/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="${pageContext.request.contextPath}/resources/admin/css/dataTables/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/resources/admin/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/resources/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<title>Admin</title>
</head>
<body>

	<div class="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			

		
			<!-- Top Navigation: Left Menu -->
			<ul class="nav navbar-nav navbar-left navbar-top-links">
				<li><a href="../"><i class="fa fa-home fa-fw"></i> Amuzon</a></li>
			</ul>

			<!-- Sidebar -->
			<div class="navbar-default sidebar">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="/admin/grafico" class="active"><i
								class="fa fa-bar-chart-o fa-fw"></i> Dashboard</a></li>
					</security:authorize>			
								
						<li><a href="/admin/orders" class="active"><i
								class="fa fa-list-ul fa-fw"></i> Orders</a></li>
								
								
						<li><a href="/admin/user" class="active"><i
								class="fa fa-users fa-fw"></i> Users</a></li>
								
					<security:authorize access="hasRole('ADMIN')">			
						<li><a href="/admin/addUser" class="active"><i
								class="fa fa-user-plus fa-fw"></i> Add User</a></li>
					</security:authorize>			
								
						<li><a href="/admin/productos" class="active"><i
								class="fa fa-product-hunt fa-fw"></i> Products</a></li>
								
					<security:authorize access="hasRole('ADMIN')">			
						<li><a href="/admin/addProduct" class="active"><i
								class="fa fa-plus fa-fw"></i> New Product</a></li>
					</security:authorize>			
								
						<li><a href="/admin/robots" class="active"><i
								class="fa fa-truck fa-fw"></i> Robots</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<tiles:insertAttribute name="content" />



</body>
</html>